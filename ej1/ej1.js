/*

Edita el archivo script.js para crear una función que haga lo siguiente:

Generar una contraseña (número entero aleatorio del 0 al 100)
Pedir al usuario que introduzca un número dentro de ese rango.
Si el número introducido es igual a la contraseña, 
aparecerá en pantalla un mensaje indicando que ha ganado; 
si no, el mensaje indicará si la contraseña en un número mayor 
o menor al introducido y dará una nueva oportunidad.
El usuario tendrá 5 oportunidades de acertar, 
si no lo consigue, aparecerá un mensaje indicando que ha perdido.

*/

"use strict";

let numIntentos = 0;

let contraseña = Math.floor(Math.random() * 101);
let contraseñaUsuario = Number(
  prompt(`Adivina un número aleatorio del 0 - 100`)
);
console.log(contraseña);

while (numIntentos <= 3 && contraseñaUsuario !== contraseña) {
  if (contraseñaUsuario < contraseña) {
    contraseñaUsuario = Number(
      prompt(`La contraseña es un número más grande (pero hasta 100)`)
    );
  } else if (contraseñaUsuario > contraseña) {
    contraseñaUsuario = Number(
      prompt(`La contraseña es un número más pequeño (pero positivo)`)
    );
  }
  numIntentos++;

  if (contraseñaUsuario === contraseña) {
    console.log(`Has acertado`);
    alert(`Has acertado!`);
  }
}

if (contraseñaUsuario !== contraseña) {
  console.log(contraseñaUsuario, contraseña);

  alert(`Has perdido`);
}
