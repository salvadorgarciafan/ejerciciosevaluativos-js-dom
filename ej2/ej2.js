"use strict";

// puntuaciones
const puntuaciones = [
  {
    equipo: "Toros Negros",
    puntos: [1, 3, 4, 2, 10, 8],
  },
  {
    equipo: "Amanecer Dorado",
    puntos: [8, 5, 2, 4, 7, 5, 3],
  },
  {
    equipo: "Águilas Plateadas",
    puntos: [5, 8, 3, 2, 5, 3],
  },
  {
    equipo: "Leones Carmesí",
    puntos: [5, 4, 3, 1, 2, 3, 4],
  },
  {
    equipo: "Rosas Azules",
    puntos: [2, 1, 3, 1, 4, 3, 4],
  },
  {
    equipo: "Mantis Verdes",
    puntos: [1, 4, 5, 1, 3],
  },
  {
    equipo: "Ciervos Celestes",
    puntos: [3, 5, 1, 1],
  },
  {
    equipo: "Pavos Reales Coral",
    puntos: [2, 3, 2, 1, 4, 3],
  },
  {
    equipo: "Orcas Moradas",
    puntos: [2, 3, 3, 4],
  },
];

// Edita para crear una función que reciba una array de objetos (equipos y puntos conseguidos)
// y muestre por consola el ** nombre ** del que más
// y del que menos puntos hayn conseguido, con sus respectivos ** totales **.
// Encontrarás un array de ejemplo en el propio documento.

function sumaMaxima(puntos) {
  const arrayTotales = puntos.map(({ equipo, puntos }) => {
    const sumaTotalPuntos = puntos.reduce((acc, n) => {
      return acc + n;
    }, 0);
    return { equipo, sumaTotalPuntos };
  });

  arrayTotales.sort((a, b) => a.sumaTotalPuntos - b.sumaTotalPuntos);

  return `El equipo con más puntos es ${
    arrayTotales[arrayTotales.length - 1].equipo
  } con ${
    arrayTotales[arrayTotales.length - 1].sumaTotalPuntos
  } puntos y el equipo con menos puntos es ${arrayTotales[0].equipo} con ${
    arrayTotales[0].sumaTotalPuntos
  } puntos.`;

  // OTRA SOLUCION:
  // const equipoMasPuntos = arrayTotales.reduce((acc, item) => {
  //   return acc.sumaTotalPuntos > item.sumaTotalPuntos ? acc : item;
  // });

  // const equipoMenosPuntos = arrayTotales.reduce((acc, item) => {
  //   return acc.sumaTotalPuntos < item.sumaTotalPuntos ? acc : item;
  // });
}

console.log(sumaMaxima(puntuaciones));
