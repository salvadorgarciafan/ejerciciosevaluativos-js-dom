"use strict";

const formatNum = (num) => {
  return num < 10 ? "0" + num : num;
};

const body = document.querySelector("body");
const clock = document.createElement("div");

body.appendChild(clock);

setInterval(() => {
  const currentDate = new Date();
  const hour = currentDate.getHours();
  const minute = currentDate.getMinutes();
  const second = currentDate.getSeconds();
  const currentHour = `${formatNum(hour)}:${formatNum(minute)}:${formatNum(
    second
  )}`;

  clock.innerHTML = currentHour;
}, 1000);
